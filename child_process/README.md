시나리오 1 : input = command line arguments, output = standard out
시나리오 2 : input = user (stdin), output = standard out 
시나리오 3 : input = file, output = another file

코드 검증 방법
참고 > 
  - cpp/prime4standalone 는 소수를 구하는 c 코드 
  - cpp/original 는 소수를 구하는 c코드 인데, main 함수를 포함. 

시나리오 1 : 
  - cd cpp/standalone_stdio/
  - node-gyp configure build
  - cpp/standalone_stdio/build/Release 에 빌드 결과 실행 파일이 생성됨. (node에서 이 프로그램을 실행함)
  - cd ../../web/
  - npm i
  - browser에서 http://localhost:3000 접속
  - standalone_args 클릭 
  - 숫자를 입력하면, 1 ~ 입력된 숫자까지 사이의 소수가 출력됨. http의 json 형태로 결과가 return 됨 

시나리오 2:
   - cd cpp/standalone_usr/
  - node-gyp configure build
  - cpp/standalone_usr/build/Release 에 빌드 결과 실행 파일이 생성됨. (node에서 이 프로그램을 실행함)
  - cd ../../web/
  - npm i
  - browser에서 http://localhost:3000 접속
  - standalone_usr 클릭 
  - 숫자를 입력하면, 1 ~ 입력된 숫자까지 사이의 소수가 출력됨. http의 json 형태로 결과가 return 됨 

 시나리오 3:
   - cd cpp/standalone_flex_file/
  - node-gyp configure build
  - cpp/standalone_flex_file/build/Release 에 빌드 결과 실행 파일이 생성됨. (node에서 이 프로그램을 실행함)
  - cd ../../web/
  - npm i
  - browser에서 http://localhost:3000 접속
  - standalone_file 클릭 
  - 숫자를 입력하면, 1 ~ 입력된 숫자까지 사이의 소수가 출력됨. http의 json 형태로 결과가 return 됨 

 

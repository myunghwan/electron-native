코드 검증 방법 
참고 > 
  - cpp/lib4ffi : shared library를 생성하는 프로젝트. cpp/prime4lib의 코드를 포함하여 빌드된다
  - prime4lib : 소수를 구하는 함수를 내포하고 있는 프로젝트, exchange를 통해 데이터를 node 로 돌려준다 

시나리오 1 : 
  - cd cpp/lib4ffi 
  - node-gyp configure build 
  - cpp/lib4ffi/build/Release 에 빌드 결과 실행 파일이 생성됨. (node에서 이 라이브러리를 링킹함)
  - cd ../../web/
  - npm i 
  - browser 에서 http://localhost:3000 접속 
  - ffi 클릭 
  - 숫자를 입력하면, 1 ~ 입력된 숫자까지 사이의 소수가 출력됨.  
